#!/usr/bin/env bash

# Variables
DBNAME=drupal_db
DBUSERNM=root
DBPASSWD=toor
PHPDIR=/etc/php/7.1
DRUPALV=8.3.7

echo -e "\n--- Ready to install... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install curl git build-essential unzip > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
apt-get -y install mysql-server > /dev/null 2>&1

echo -e "Create database"
mysql -u $DBUSERNM -p$DBPASSWD -e "CREATE DATABASE $DBNAME DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"

echo -e "\n--- Setting document root to public directory ---\n"
apt-get install -y apache2 > /dev/null 2>&1

# https://www.digitalocean.com/community/tutorials/how-to-upgrade-to-php-7-on-ubuntu-14-04
apt-get install -y language-pack-en-base > /dev/null 2>&1
LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y > /dev/null 2>&1

apt-get -qq update

apt-get -y install php7.1 php7.1-curl php7.1-gd php7.1-mcrypt php7.1-mysql php7.1-apcu php7.1-xml php7.1-mbstring > /dev/null 2>&1

echo -e "\n--- Installing PHP-specific packages ---\n"

echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite > /dev/null 2>&1

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

if ! [ -d /vagrant/www ]; then
    mkdir /vagrant/www
  if ! [ -d /vagrant/www/html ]; then
    mkdir /vagrant/www/html
  fi
fi

if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant/www /var/www
fi

cd /vagrant/www/html
wget http://ftp.drupal.org/files/projects/drupal-$DRUPALV.tar.gz > /dev/null 2>&1
tar -xvzf drupal-$DRUPALV.tar.gz > /dev/null 2>&1
rm drupal-$DRUPALV.tar.gz
cp -r drupal-$DRUPALV/. ./
rm -r drupal-$DRUPALV

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" $PHPDIR/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" $PHPDIR/apache2/php.ini

echo -e "\n--- Installing Composer for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Use vagrant user and group on apache ---\n"
sed -i 's/User ${APACHE_RUN_USER}/User vagrant/g' /etc/apache2/apache2.conf
sed -i 's/Group ${APACHE_RUN_GROUP}/Group vagrant/g' /etc/apache2/apache2.conf

echo -e "\n--- Restarting Apache ---\n"
service apache2 restart > /dev/null 2>&1

echo -e "\n--- Installing Drush ---\n"
wget -O /usr/local/bin/drush https://s3.amazonaws.com/files.drush.org/drush.phar > /dev/null 2>&1
chmod +x /usr/local/bin/drush


